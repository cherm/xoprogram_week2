import java.util.Scanner;

public class OX {

	public static char[][] board = { { '1', '2', '3' }, { '4', '5', '6' }, { '7', '8', '9' } };

	public static int winner, player = 1, go, row, column, line = 0;

	public static void showWelcome() {
		System.out.printf("\t\t\t    Welcome to Game OX.");
	}

	public static void check() {
		if ((board[0][0] == board[1][1] && board[0][0] == board[2][2])
				|| (board[0][2] == board[1][1] && board[0][2] == board[2][0])) {
			winner = player;
		} else {
			for (line = 0; line <= 2; line++) {
				if ((board[line][0] == board[line][1] && board[line][0] == board[line][2])
						|| (board[0][line] == board[1][line] && board[0][line] == board[2][line])) {
					winner = player;
				}

			}
		}
	}

	public static void input() {
		Scanner kb = new Scanner(System.in);
		do {
			System.out.println("");
			System.out.printf("\n\t\t Player %d, please enter the number [your] %c : ", player,
					(player == 1) ? 'X' : 'O');

			go = kb.nextInt();
			row = --go / 3;
			column = go % 3;
		} while (go < 0 || go > 9 || board[row][column] > '9');
		board[row][column] = (player == 1) ? 'X' : 'O';
	}

	public static void showBoard() {

		System.out.println("\n");
		System.out.printf("\t\t\t\t %c | %c | %c\n", board[0][0], board[0][1], board[0][2]);
		System.out.print("\t\t\t\t---+---+---\n");
		System.out.printf("\t\t\t\t %c | %c | %c\n", board[1][0], board[1][1], board[1][2]);
		System.out.print("\t\t\t\t---+---+---\n");
		System.out.printf("\t\t\t\t %c | %c | %c\n", board[2][0], board[2][1], board[2][2]);

	}

	public static void showWinner() {
		showBoard();
		if (winner == 0) {
			System.out.printf("\n\t\t\tHow boring, it is a draw\n");
		} else {
			System.out.printf("\n\t\t\"Congratuiations\", player %d, YOU ARE THE WINNER!\n", winner);
		}
	}

	public static void SwitchPlayer() {
		if (player==1) {
			player +=1;
		} else {
			player -=1;
		}
	}

	public static void main(String[] args) {

		showWelcome();
		for (int i = 0; i < 9 && winner == 0; i++) {
			showBoard();
			input();
			check();
			SwitchPlayer();
		}
		showWinner();

	}

}